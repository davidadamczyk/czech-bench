# Dataset Attributions

All included datasets are obtained and redistributed in good faith. Dataset sources and attributions are provided in their respective README files. Original dataset licenses are also included if they were specified.

Only the subsets used for evaluation are redistributed in this repository to ensure result reproducibility. The full original datasets can be obtained using the links in their respective README files.